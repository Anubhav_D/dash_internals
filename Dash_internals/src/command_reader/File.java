package command_reader;

import java.awt.Desktop;
import java.io.IOException;
import java.util.Date;
import java.util.Scanner;

public class File {
	public static boolean isPathExist(String str,User user){
		String path=user.cdir+"\\"+str;
		if(new java.io.File(path).exists()){
			user.setCdir(path);
			return true;
		}
		
		return false;
	}
public static void open(String str,User user){
	if(str==null){
		System.out.println("file name not specified . Syntax is: open file_name.extension");
	}
	String absolute_path=user.getCdir()+"\\"+str;
	if(!new java.io.File(absolute_path).exists()){
		System.out.println("File does not exists");
		return;
	}
	if(new java.io.File(absolute_path).isFile()){
	if(Desktop.isDesktopSupported()){
		java.io.File file=new java.io.File(absolute_path);
		try{
		Desktop.getDesktop().open(file);
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}	
}
	else{
		System.out.println("File doesn't exists");
	}
}
public static String formatUsedSpace(long temp){
	String str= "Size:";
	String array[]={"bytes","KB","MB","GB","TB"};
	int count=0;
	while(temp>1024){
		temp%=1024;
		count++;
	}
	str+=temp+" "+array[count];
	return str;
}
public static void ls(User user){
	java.io.File folder=new java.io.File(user.cdir);
	java.io.File[] files=folder.listFiles();
	int fcount=0,dcount=0;
	for(int i=0;i<files.length;i++){
		if(files[i].isFile()){
		System.out.println("File:       "+files[i].getName()+"  Last Modified:"+new Date(files[i].lastModified())+" File"+File.formatUsedSpace(files[i].length()));
		fcount++;}
		else{
			System.out.println("Directory:  "+files[i].getName());
			dcount++;
		}
	}
	System.out.println(fcount+" files found and "+dcount+" directory found.");
}

public static void mk(String str,User user){
	String path=user.cdir+"\\"+str;
	if(new java.io.File(path).exists()){
		System.out.println("File already exists");
		System.out.print("Do you want to replace it [y/n]:");
		Scanner in=new Scanner(System.in);
		if(in.next().equalsIgnoreCase("y")){
			java.io.File f=new java.io.File(path);
			try{
				f.delete();
			if(f.createNewFile())System.out.println("File created Successfully");
			else System.out.println("Some error occured");
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		else{
			return;
		}
	}
	else{
		java.io.File f=new java.io.File(path);
		try{
		if(f.createNewFile())System.out.println("File created Successfully");
		else System.out.println("Some error occured");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}

public static void mkdir(String str,User user){
	String path=user.cdir+"//"+str;
	if(new java.io.File(path).exists()){
		System.out.println("Directory already exists");
		return;
	}
	java.io.File f=new java.io.File(path);
	if(f.mkdirs()){
		System.out.println("Directory created Successfully");
	}
	else{
		System.out.println("Some Error Occured");
	}
}

public static void rm(String str,User user){
	String path=user.cdir+"//"+str;
	if(new java.io.File(path).exists()){
		java.io.File f=new java.io.File(path);
		if(f.delete()){
			System.out.println("File deleted Successfully");
		}
		else System.out.println("Some error occured");
	}
	else{
		System.out.println("File doesn't exist");
	}
}

public static void rmdir(String str,User user){
	String path=user.cdir+"//"+str;
	if(new java.io.File(path).exists()){
		java.io.File f=new java.io.File(path);
		try{
		if(f.delete()){
			System.out.println("Directory is deleted successfully");
		}
		else{
			System.out.println("Look's like directory is not empty");
		}
		}
		catch(Exception e){System.out.println("Exception");}
	}
}
}
