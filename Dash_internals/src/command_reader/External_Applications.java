package command_reader;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Scanner;

public class External_Applications {

	public static void javac(String str,User user){
		String path=user.cdir+"\\"+str;
		if(new java.io.File(path).exists()){
			try{
			Process p=Runtime.getRuntime().exec("javac "+path);
			p.waitFor();
			BufferedReader br=new BufferedReader(new InputStreamReader(p.getErrorStream()));
			String line=null;
			while((line=br.readLine())!=null){
				System.out.println(line);
			}
			
		}
			catch(IOException e){
				System.out.println("Exception");
				e.printStackTrace();
				}
			catch(InterruptedException e){
				System.out.println("Exception");
				e.printStackTrace();
			}
			}
		else{
			System.out.println("File doesn't exists");
		}
	}
	public static void java(String str,User user){
		String path=user.getCdir()+"\\"+str;
		if(new java.io.File(path+".class").exists()){
			try{
			Process p=Runtime.getRuntime().exec("java -cp "+user.cdir+" "+str);
			p.waitFor();
			BufferedReader br=new BufferedReader(new InputStreamReader(p.getInputStream()));
			BufferedReader er=new BufferedReader(new InputStreamReader(p.getErrorStream()));
			String line=null;
			while((line=br.readLine())!=null){
				System.out.println(line);
			}
			while((line=er.readLine())!=null){
				System.out.println(line);
			}
			}
			catch(Exception e){
				e.printStackTrace();
			}
			}
		else{
			System.out.println(".class file doesn't exist");
		}
	}
	
}
