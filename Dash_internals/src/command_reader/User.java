package command_reader;

import java.nio.file.Files;

public class User {
public String cdir;
public boolean session=true;

public void setCdir(String str){
	if(str==null){
		System.out.println("Directory not specified");
		return ;
	}
	if(!new java.io.File(str).exists()){
		System.out.println("Path doesn't exists");
		return;
	}
	this.cdir=str;
}
public String getCdir(){
	return this.cdir;
	}
}
