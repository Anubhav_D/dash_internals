package command_reader;

import java.util.ArrayList;
import java.util.regex.Pattern;

public class Mathematics {
public static boolean isExpression(String str){
	if(Pattern.matches("[0-9+-/*()]*", str)){
		return true;
	}
	return false;
}
public static void expression_Solver(String str){
	String st=toPostfix(str);
	System.out.println(st);
	ArrayList<Integer> stack=new ArrayList<Integer>();
	int top=-1;
	double result=0.0;
	for(int i=0;i<st.length();i++){
		char c=st.charAt(i);
		if(c=='+' || c=='-' || c=='*' || c=='/'){
			double v1=stack.get(top);
			double v2=stack.get(top-1);
			top-=2;
			switch(c){
			case '+':
				result+=v1+v2;
				break;
			
			case '-':
				result+=v2-v1;
				break;
				
			case '*':
				result+=v2*v1;
				break;
				
			case '/':
				result+=v2/v1;
				break;
			}
			
		}
		else{
			stack.add((int)c-(int)'0');
			top++;
		}
	}
	System.out.println(result);
}
public static int precedenceOf(char c){
	int i=0;
	if(c=='*')i=1;
	else if(c=='/')i=2;
	return i;
}
public static String toPostfix(String str){
	StringBuilder st=new StringBuilder();
	char precedence[]={'*','/'};
	ArrayList<Character> stack=new ArrayList<Character>();
	int top=-1;
	for(int i=0;i<str.length();i++){
		char c=str.charAt(i);
		if(c!='+'&& c!='*' && c!='-' && c!='/'){
			st.append(c);
		}
		else{
			System.out.println(c);
			int pre=precedenceOf(c);
			boolean flag=true;
			while(flag){
				if(top==-1){
					stack.add(c);
					top++;
					flag=false;
				}
				else if(precedenceOf(c)<=precedenceOf(stack.get(top))){
					st.append(stack.get(top));
					stack.remove(top);
					top--;
				}
				else{
					stack.add(c);
					top++;
					flag=false;
				}
			}
		}
	}
	while(top>=0){
		st.append(stack.get(top));
		top--;
	}
	return st.toString();
}
}
