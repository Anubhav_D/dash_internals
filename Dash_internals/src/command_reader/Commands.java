package command_reader;

import java.util.Date;

public class Commands {
public static String array[]={"open","time","date","cd","exit","ls","javac","java","mk","mkdir","rm","rmdir","back"};
public static boolean isCommand(String st){
	for(int i=0;i<array.length;i++){
		if(st.equalsIgnoreCase(array[i])){
			return true;
		}
	}
	return false;
}
public static void execute(String str,String parameter,User user){
	switch(str){
	case "open":
		command_reader.File.open(parameter,user);
		break;
	
	case "cd":
		user.setCdir(parameter);
		break;
		
	case "ls":
		File.ls(user);
		break;
	
	case "mk":
		File.mk(parameter, user);
		break;
		
	case "mkdir":
		File.mkdir(parameter, user);
		break;
		
	case "rm":
		File.rm(parameter, user);
		break;
		
	case "rmdir":
		File.rmdir(parameter, user);
		break;
		
	case "back":
		Directions.back(user);
		break;
		
	case "javac":
		External_Applications.javac(parameter, user);
		break;
		
	case "java":
		External_Applications.java(parameter, user);
		break;
		
	case "time":
		Date d=new Date();
		System.out.println(new Date(d.getTime()));
		break;
	
	case "date":
		Date d2=new Date();
		System.out.println(d2.getDate()+"/"+d2.getMonth()+"/"+(d2.getYear()+1900));
		break;
	case "exit":
		user.session=false;
		break;
		
	}
}
}
