package command_reader;
import java.util.Scanner;
public class Main {
public static void main(String args[]){
	Scanner in=new Scanner(System.in);
	User user=new User();
	user.setCdir(System.getProperty("user.dir"));
	while(user.session){	
	System.out.print(user.getCdir()+">>");
	String array[]=command_reader.Token_Analyser.getTokens(in.nextLine());
	if(array[0].equals(""))continue;
	String parameter;
	if(array.length<2){
		parameter=null;
		if(File.isPathExist(array[0], user))continue;
	}
	else parameter=array[1];
	if(command_reader.Commands.isCommand(array[0])){
		String command=array[0];
		command_reader.Commands.execute(command,parameter,user);
	}
	else if(Mathematics.isExpression(array[0]))System.out.println("Expression identified");
	else{
		System.out.println("Command not found");
	}
	
	}
}
}
